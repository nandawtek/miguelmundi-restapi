const assert = require('assert')
const path = require('path')

const checkEnvVars = () => {
  try {
    assert.ok(process.env.NODE_ENV)
    assert.ok(process.env.RESTAPI_PORT)
    assert.ok(process.env.RESTAPI_VERSION)
  } catch (err) {
    throw new Error(`Set the Environment Variables needed!\n${err}`)
  }
}

const loadEnvVars = () => {
  const location = path.join(__dirname, '/../.env')
  require('dotenv').config({ path: location })

  checkEnvVars()
}

module.exports = {
  checkEnvVars,
  loadEnvVars
}
