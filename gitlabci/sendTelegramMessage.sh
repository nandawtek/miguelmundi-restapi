#!/bin/sh

chatId="$1"
telegramToken="$2"
status="$3"
disableNotification="$4"

statusMessage="%F0%9F%98%A1%20${CI_PROJECT_NAME}%20Test%20failed!"

if [ "$status" = "OK" ]
  then 
    statusMessage="%F0%9F%98%8D%20${CI_PROJECT_NAME}%20Test%20passed!"
fi

curl \
  -X POST \
  -H "Content-Type: application/json" \
  -d "{\"chat_id\": \"${chatId}\", \"disable_notification\": \"${disableNotification}\"}" \
  https://api.telegram.org/bot${telegramToken}/sendMessage?text=$statusMessage
