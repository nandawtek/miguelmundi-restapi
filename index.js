const express = require('express')
const app = express()
const healthzRoutes = require('./routes/healthz')

app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Methods', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  next()
})
const apiVersion = `/${process.env.RESTAPI_VERSION}`
app.use(apiVersion, healthzRoutes)

module.exports = app
