const Healthz = require('../../services/healthz')

class HealthzAction {
    static async invoke () {
        return Healthz.service.isHealthzy()
    }
}

module.exports = HealthzAction