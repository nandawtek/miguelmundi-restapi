const express = require('express')
const router = express.Router()
const Action = require('../actions/healthz')
const SUCCESS = 200
const BAD_REQUEST = 400

router.get('/healthz', async (req, res) => {
    try {
        const response = await Action.healthz.invoke()
        res.status(SUCCESS).send(response)
    } catch (error) {
        console.log(error.message, error.stack)
        res.status(BAD_REQUEST).send({ status: 'error', message: error.message })
    }
})

module.exports = router
