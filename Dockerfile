FROM node:14.17.6-alpine3.14

ENV RESTAPI_PORT 8080

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE ${RESTAPI_PORT}

CMD ["node", "."]
