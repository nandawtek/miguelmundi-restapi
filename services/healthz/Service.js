const Healthz = require('../../domain/healthz/Healthz')

class Service {
    static async isHealthzy () {
        const healthz = new Healthz()
        return { status: healthz.getStatus() }
    }
}

module.exports = Service
