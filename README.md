## Rest API Boilerplate

This is a Rest API boilerplate made by some devscola mates.

### Prepare

`npm install`

### Start

`npm start`

### Test

`npm test`

## CI/CD

We use Gitops, it means that we use gitlab with the `.gitlba-cy.yml` file to test and build our docker image
We use a telegram bot to notify pipelines status
Here some interesting websites to create our telegram bot

* [Create a bot](https://sendpulse.com/knowledge-base/chatbot/create-telegram-chatbot)
* [Send Message API](https://core.telegram.org/bots/api#sendmessage)
* [Send Message with curl](https://thecodersblog.com/sending-the-message-on-telegram-using-cUrl/)

### Deploy

We execute deploys only when all pipelines passed, we have a versioned registry image to that purpose.
We execute the app in the custom environment passing to it the env variables, with a command similar to that after pull our image from our gitlab registry

1. `sudo docker pull registry.gitlab.com/nandawtek/miguelmundi-restapi:1.0.0`
2. `sudo docker run -e NODE_ENV=production -e RESTAPI_VERSION=v1 -e RESTAPI_PORT=8080 -p 8080:8080 c20453832fdc`

### Resilence

We want to use our app bot, using /v1/healthz route to ensure our app is healthy. In wrong cases we try to up the app with our bot.
