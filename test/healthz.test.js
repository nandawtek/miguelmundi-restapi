require('../utils/environment').loadEnvVars()
const request = require('supertest')
const app = require('../index')
const SUCCESS = 200

describe('The API', () => {
  test('is available', async () => {
    const response = await request(app).get(`/${process.env.RESTAPI_VERSION}/healthz`)
    
    expect(response.statusCode).toBe(SUCCESS)
    expect(response.body.status).toBe('ok')
  })
})
